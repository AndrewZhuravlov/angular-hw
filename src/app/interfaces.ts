export interface Task {
  taskText: string;
  likes: number;
  comments: string[];
}
