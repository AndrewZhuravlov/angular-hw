import { BoardService } from './services/board.service';
import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'retro';
  constructor(private boardService :BoardService){};
  @HostListener('window:beforeunload')
  beforeUnload() {
    this.boardService.setDataToStorage(this.boardService.retroBoardState);
  }
}
