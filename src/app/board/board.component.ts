import { Task } from './../interfaces';
import { BoardService } from './../services/board.service';
import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})

export class BoardComponent implements OnInit {
  columns;
  columnNames;
  constructor(
    private boardService: BoardService,
    ) { }

  ngOnInit(): void {
    this.boardService.retroBoardColumns$
      .subscribe(columns => {
        this.columns = columns;
        this.columnNames = Object.keys(columns);
      });
      
      this.boardService.getDataFromStorage();  
  }

  createTask(ev,name) {
    let taskValue = ev.target.value;
    if(!taskValue) return;
    
    const task: Task = {
      taskText: taskValue,
      likes: 0,
      comments: []
    }

    this.boardService.addTask(name, task);
    ev.target.value = '';
  }

  addLike(name, text) {
    this.boardService.addLikeToTask(name, text)
  }

  drop(event: CdkDragDrop<[Task]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }
}

