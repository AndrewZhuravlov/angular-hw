import { Task } from './../interfaces';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  stateName = 'RetroState';
  retroBoardColumns$ = new Subject<{}>();
  retroBoardState = {};

  addColumn(columnName) {
    this.retroBoardState[columnName] = [];
    this.retroBoardColumns$.next(this.retroBoardState);
  }

  addTask(columnName, task: Task) {
    this.retroBoardState[columnName].push(task);
    this.retroBoardColumns$.next(this.retroBoardState);
  }

  addComment(columnName, taskText, commentText) {
    const idx = this.retroBoardState[columnName]
      .findIndex(item => item.taskText === taskText);
    this.retroBoardState[columnName][idx]['comments'].push(commentText);
    this.retroBoardColumns$.next(this.retroBoardState);
  }

  addLikeToTask(columnName, taskText) {
    const idx = this.retroBoardState[columnName]
      .findIndex(item => item.taskText === taskText);
    this.retroBoardState[columnName][idx]['likes'] = this.retroBoardState[columnName][idx]['likes'] + 1;
    this.retroBoardColumns$.next(this.retroBoardState);
  }

  getDataFromStorage() {
    this.retroBoardState = JSON.parse(localStorage.getItem(this.stateName)) || {};
    this.retroBoardColumns$.next(this.retroBoardState);
  }

  // isRetroStateCreated() {
  //   return this.getDataFromStorage() ? true : false;
  // }

  setDataToStorage(state) {
    localStorage.setItem(this.stateName, JSON.stringify(state));
  }
}
