import { BoardService } from './../services/board.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  headerValue = '';
  constructor(private boardService: BoardService) { }

  ngOnInit(): void {
  }
  onColumnNameEnter() {
    if(!this.headerValue) return;
    this.boardService.addColumn(this.headerValue);
    this.headerValue = '';
  }
}
