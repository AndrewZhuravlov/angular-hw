import { BoardService } from './../services/board.service';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent {
  @Input() columnName:string;
  @Input() text:string;

  closeResult: string;
  value: string = '';
  modalRef: NgbModalRef

  constructor(
    private modalService: NgbModal,
    private boardService: BoardService,
  ) { }

  openVerticallyCentered(content) {
    this.modalRef = this.modalService.open(content, { centered: true });
  }

  createComment() {
    this.boardService.addComment(this.columnName, this.text, this.value)
    this.value = '';
    this.modalRef.close();
  }
}
